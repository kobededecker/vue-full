import Vue from "vue";
import VueRouter from "vue-router";

import view1 from './components/view1'; 
import view2 from './components/view2'; 

Vue.use(VueRouter)

let routes = [
    {"path": "/comp1", "component": view1},
    {"path": "/comp2", "component": view2},
]

export default new VueRouter({
    routes
})