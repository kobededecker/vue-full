import Vue from 'vue'
import App from "./app";

import store from './store'
import router from './routes'

export default new Vue({
    el: "#app",
    render: h => h(App),
    store,
    router
})